#!/bin/bash
set -x DEBIAN_FRONTEND=noninteractive
cat > /etc/apt/apt.conf.d/99custom <<CONF
APT
{
    GET
    {
        Assume-Yes "true";
    };
};
CONF
apt-get update
apt-get install --yes --no-install-recommends ca-certificates nodejs yarnpkg pkg-config libssl-dev libc-dev curl gcc binutils zip

bash /rust-init -y --default-toolchain nightly
source $HOME/.cargo/env
rustup default nightly
rustup target add wasm32-unknown-unknown
rustup component add clippy-preview
rustup component add rustfmt-preview
cargo install wasm-pack
cargo install grcov
