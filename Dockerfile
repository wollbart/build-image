FROM ubuntu:disco

ADD /rust-init /
ADD /install.sh /
RUN ["/bin/bash","/install.sh"]
ENV PATH="/root/.cargo/bin:$PATH"
